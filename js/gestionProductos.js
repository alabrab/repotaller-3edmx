var productosObtenidos;



function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products"
  var request = new XMLHttpRequest();

  request.onreadystatechange = function (){
      if (this.readyState == 4 && this.status == 200 ){
        //console.table(JSON.parse(request.responseText).value);
        productosObtenidos = request.responseText;
        procesarProductos();
      }
    }
    request.open("GET", url, true);
    request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName + '');
  var divTable = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columaNombre = document.createElement("td");
    columaNombre.innerText = JSONProductos.value[i].ProductName;

    var columaPrecio = document.createElement("td");
    columaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columaStock = document.createElement("td");
    columaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columaNombre);
    nuevaFila.appendChild(columaPrecio);
    nuevaFila.appendChild(columaStock);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
